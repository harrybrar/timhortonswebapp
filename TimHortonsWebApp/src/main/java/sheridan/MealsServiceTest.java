package sheridan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	MealsService mealsService = new MealsService();

    List<String> types = new ArrayList<String>( );

	@Test
	public void testDrinksExceptional() {
		types = mealsService.getAvailableMealTypes(null);
		assertTrue(types.contains("No Brand Available"));
	}
	
	@Test
	public void testDrinksRegular() {
		types = mealsService.getAvailableMealTypes(MealType.DRINKS);
		assertFalse(types.isEmpty());
	}
	
	
	@Test
	public void testDrinksBoundaryIn() {
		types = mealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue(types.size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		types = mealsService.getAvailableMealTypes(null);
		assertFalse(types.size() > 1);
	}
}
